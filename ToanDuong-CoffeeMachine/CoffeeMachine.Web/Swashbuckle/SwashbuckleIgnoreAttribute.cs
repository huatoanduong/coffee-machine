﻿using System;

namespace CoffeeMachine.Web.Swashbuckle
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SwashbuckleIgnoreAttribute : Attribute
    {
    }
}
