﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CoffeeMachine.Web.Filters
{
    public class AprilFoolFilter : ActionFilterAttribute
    {
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var now = DateTimeOffset.UtcNow;
            if (now.Month == 4 && now.Day == 1)
            {
                context.Result = new StatusCodeResult(418);
            }
            else
            {
                await next();
            }
        }
    }
}
