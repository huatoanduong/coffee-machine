﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CoffeeMachine.Core.Common.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace CoffeeMachine.Web.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, IWebHostEnvironment hostingEnvironment, ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                await HandleExceptionAsync(context, ex, _hostingEnvironment, false);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception, IWebHostEnvironment hostingEnvironment, bool htmlEncode)
        {
            // set code
            var code = (int)HttpStatusCode.InternalServerError; // 500 if unexpected

            if (exception is HttpStatusCodeException statusCodeException)
            {
                code = statusCodeException.HttpStatusCode;
            }

            context.Response.StatusCode = code;
            return context.Response.WriteAsync(string.Empty);
        }
    }
}
