﻿using CoffeeMachine.Core.Common.Configurations;
using CoffeeMachine.Core.Common.Exceptions;
using CoffeeMachine.Core.Common.Providers;
using CoffeeMachine.Web.Exceptions;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace CoffeeMachine.Web.Extensions
{
    public static class RegisterServices
    {
        public static IServiceCollection AutoInjectServices(this IServiceCollection services)
        {
            services
                .WithScopedLifetime<ClientApiError>() //register for whole assembly
                .WithScopedLifetime<ErrorItem>();

            services.AddSingleton<ICoffeeTankProvider, CoffeeTankProvider>();

            services.AddMediatR(typeof(ICoffeeTankProvider));

            services.AddHttpContextAccessor();

            services.AddHttpClient();

            return services;
        }

        public static IServiceCollection RegisterConfiguration(this IServiceCollection services, IConfiguration Configuration)
        {
            // configure strongly typed settings objects
            services.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));
            services.Configure<WeatherApiSettings>(Configuration.GetSection(nameof(WeatherApiSettings)));

            return services;
        }

        public static IServiceCollection RegisterLogger(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddLogging(loggingBuilder =>
                {
                    // configure Logging with NLog
                    loggingBuilder.ClearProviders();
                    loggingBuilder.SetMinimumLevel(LogLevel.Trace);
                    loggingBuilder.AddNLog(configuration);
                });

            return services;
        }
    }
}
