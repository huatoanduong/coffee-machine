﻿using System.Collections.Generic;
using System.Net;
using CoffeeMachine.Core.Common.Exceptions;
using CoffeeMachine.Web.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ToanDuong_CoffeeMachine.Controllers
{
    [ApiController]
    [Authorize]
    [Produces("application/json")]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.Conflict)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.Forbidden)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.InternalServerError)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.MethodNotAllowed)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.Unauthorized)]
    public abstract class BaseController : ControllerBase
    {
        protected readonly IMediator _mediator;

        protected BaseController(
            IMediator mediator
        )
        {
            _mediator = mediator;
        }

        protected IMediator Mediator => _mediator;

        protected ActionResult OkOrNoContent(object value)
        {
            if (value == null) return NoContent();

            return Ok(value);
        }

        protected ActionResult NullSafeOk<T>(List<T> list)
        {
            if (list == null) return Ok(new List<T>());

            return Ok(list);
        }

        protected ActionResult NullSafeOk<T>(IList<T> list)
        {
            if (list == null) return Ok(new List<T>());

            return Ok(list);
        }

        protected ActionResult NullSafeOk(object value)
        {
            if (value == null) throw new NotFoundException();

            return Ok(value);
        }

        protected ActionResult NullSafeOk() => new OkResult();
    }
}
