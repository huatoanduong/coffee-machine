﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoffeeMachine.Core.CoffeeMaker.Requests;
using CoffeeMachine.Web.Filters;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ToanDuong_CoffeeMachine.Controllers
{
    [Route("brew-coffee")]
    public class BrewCoffeeController : BaseController
    {
        public BrewCoffeeController(
            IMediator mediator
        ) : base(mediator)
        {
        }

        [AprilFoolFilter] // We can use here, or use a global filter with the same implementation
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> MakeCoffee(CancellationToken cancellationToken)
        {
            var response = await _mediator.Send(new MakeCoffee.Request(), cancellationToken);
            return OkOrNoContent(response);
        }
    }
}
