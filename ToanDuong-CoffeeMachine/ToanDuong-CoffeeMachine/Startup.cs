using System.Linq;
using CoffeeMachine.Core.Common.Configurations;
using CoffeeMachine.Web.Extensions;
using CoffeeMachine.Web.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ToanDuong_CoffeeMachine
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            CurrentEnvironment = env;
        }

        public bool IsDevelopment => CurrentEnvironment.IsDevelopment();

        private IWebHostEnvironment CurrentEnvironment { get; set; }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Just for development
            services.AddCors();

            services.AddOptions();
            services.AutoInjectServices()
                .RegisterConfiguration(Configuration)
                .RegisterLogger(Configuration);

            if (IsDevelopment)
            {
                services.AddSwaggerDocumentation();
            }

            ConfigureMvc(services);

            services.AddResponseCompression();
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);

            // configure jwt authentication
            var appSettings = Configuration.GetSection(nameof(AppSettings)).Get<AppSettings>();
            var tokenSettings = appSettings.TokenSettings;
            services.AddAuthentication(opt =>
                {
                    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    //x.RequireHttpsMetadata = false;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidIssuer = tokenSettings.Issuer,
                        ValidAudience = tokenSettings.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(tokenSettings.SecretBytes)
                    };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //Just for development
            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .SetIsOriginAllowed(host => true)
                .AllowCredentials()
            );

            if (IsDevelopment)
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerDocumentation();
            }

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        protected virtual void ConfigureMvc(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    var jsonSerializerSettings = options.SerializerSettings;
                    ////Remove nulls from payload and save bytes
                    jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    // Make json output camelCase
                    jsonSerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });
        }
    }
}
