﻿using System;
using System.IO;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoffeeMachine.Core.OpenWeatherMap.Services;
using CoffeeMachine.Tests.MockServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Xunit;

namespace CoffeeMachine.Tests.Fixtures
{
    public class HttpFixture : IDisposable, IAsyncLifetime
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        public IConfigurationRoot Configuration { get; }

        public string AccessToken { get; protected set; }

        public IServiceProvider ServiceProvider { get; protected set; }

        public IServiceCollection Services { get; private set; }

        public HttpFixture()
        {
            var fullPath = Path.GetFullPath(@"../../../");
            var baseAddress = "https://localhost:44357";

            var configuration = new ConfigurationBuilder()
                .SetBasePath(fullPath)
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile("appsettings.Development.json", optional: true)
                .Build();

            var whb = new WebHostBuilder()
                .UseUrls(baseAddress)
                .UseContentRoot(fullPath)
                .UseStartup<TestServerStartup>()
                .UseConfiguration(configuration)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var settings = config.Build();
                })
                .ConfigureTestServices(s =>
                {
                    s.AddScoped<IMockOpenWeatherMapService, MockOpenWeatherMapService>();

                    //New implementation for returning mock data, or we can have option Func<>
                    s.AddScoped<IOpenWeatherMapService>(serviceProvider =>
                    {
                        var implementing = serviceProvider.GetService<IMockOpenWeatherMapService>();
                        return implementing;
                    });



                    Services = s;
                });

            _server = new TestServer(whb);
            _server.BaseAddress = new Uri(baseAddress);

            _client = _server.CreateClient();
            _client.BaseAddress = new Uri(baseAddress);

            Configuration = configuration;

            ServiceProvider = Services.BuildServiceProvider();
        }

        public async Task<T> GetAsync<T>(string url)
        {
            var response = await _client.GetAsync(url);
            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(stringResponse);
        }
        
        public async Task<HttpResponseMessage> GetRawAsync(string url)
        {
            var response = await _client.GetAsync(url);
            return response;
        }
        
        #region IDisposable Support

        private bool disposedValue; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _client.Dispose();
                    _server.Dispose();
                }

                disposedValue = true;
            }
        }


        // This code added to correctly implement the disposable pattern.
        public virtual void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion

        public virtual Task InitializeAsync()
        {
            return Task.CompletedTask;
        }

        public virtual Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

    }
}