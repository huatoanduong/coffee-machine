﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using ToanDuong_CoffeeMachine;

namespace CoffeeMachine.Tests.Fixtures
{
    public class TestServerStartup : Startup
    {
        public TestServerStartup(IConfiguration configuration, IWebHostEnvironment env) : base(configuration, env)
        {
        }
    }
}