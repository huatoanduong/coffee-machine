﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoffeeMachine.Core.OpenWeatherMap.Models;
using CoffeeMachine.Core.OpenWeatherMap.Services;

namespace CoffeeMachine.Tests.MockServices
{
    public interface IMockOpenWeatherMapService : IOpenWeatherMapService
    {
        //These are 2 approaches for set mock data in unit test
        // 1/ New method to return the mock data
        // 2/ Or we can use Func<OpenWeatherCityResponseModel>
        void SetResponse(OpenWeatherCityResponseModel response);
    }

    public class MockOpenWeatherMapService : IMockOpenWeatherMapService
    {
        private OpenWeatherCityResponseModel Response { get; set; } = new OpenWeatherCityResponseModel
        {
            Main = new Main
            {
                Temp = 30
            }
        };

        public Task<OpenWeatherCityResponseModel> GetCurrentWeatherAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Response);
        }

        public void SetResponse(OpenWeatherCityResponseModel response)
        {
            Response = response;
        }
    }
}
