﻿using System.Linq;
using System.Threading.Tasks;
using CoffeeMachine.Core.CoffeeMaker.Requests;
using CoffeeMachine.Core.OpenWeatherMap.Models;
using CoffeeMachine.Tests.Fixtures;
using CoffeeMachine.Tests.MockServices;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace CoffeeMachine.Tests.TestCases
{
    public class HotColdCoffeeShould : IClassFixture<HttpFixture>
    {
        private readonly HttpFixture _fixture;

        public HotColdCoffeeShould(HttpFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task HotCoffee()
        {
            var mockOpenWeatherMapService = _fixture.ServiceProvider.GetService<IMockOpenWeatherMapService>();
            mockOpenWeatherMapService.SetResponse(new OpenWeatherCityResponseModel
            {
                Main = new Main
                {
                    Temp = 20
                }
            });

            var mediator = _fixture.ServiceProvider.GetService<IMediator>();

            var response = await mediator.Send(new MakeCoffee.Request());

            Assert.NotNull(response);
            Assert.Equal("Your piping hot coffee is ready", response.Message);
        }

        [Fact]
        public async Task ColdCoffee()
        {
            var mockOpenWeatherMapService = _fixture.ServiceProvider.GetService<IMockOpenWeatherMapService>();
            mockOpenWeatherMapService.SetResponse(new OpenWeatherCityResponseModel
            {
                Main = new Main
                {
                    Temp = 40
                }
            });

            var mediator = _fixture.ServiceProvider.GetService<IMediator>();

            var response = await mediator.Send(new MakeCoffee.Request());

            Assert.NotNull(response);
            Assert.Equal("Your refreshing iced coffee is ready", response.Message);
        }
    }
}
