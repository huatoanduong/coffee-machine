﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CoffeeMachine.Core.CoffeeMaker.Models;
using CoffeeMachine.Tests.Fixtures;
using Xunit;

namespace CoffeeMachine.Tests.TestCases
{
    public class FiveRunsShould : IClassFixture<HttpFixture>
    {
        private readonly HttpFixture _fixture;

        public FiveRunsShould(HttpFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task FiveRuns()
        {
            for (int i = 0; i < 4; i++)
            {
                var actualResult = await _fixture.GetAsync<CupOfCoffeeModel>("/brew-coffee");

                Assert.NotNull(actualResult);
                Assert.Equal("Your piping hot coffee is ready", actualResult.Message);
            }

            var response = await _fixture.GetRawAsync("/brew-coffee");

            Assert.Equal(HttpStatusCode.ServiceUnavailable, response.StatusCode);
        }
    }
}
