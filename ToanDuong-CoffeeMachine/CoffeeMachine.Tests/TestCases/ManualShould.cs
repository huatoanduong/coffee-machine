﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoffeeMachine.Core.CoffeeMaker.Models;
using CoffeeMachine.Tests.Fixtures;
using Xunit;

namespace CoffeeMachine.Tests.TestCases
{
    public class ManualShould : IClassFixture<HttpFixture>
    {
        private readonly HttpFixture _fixture;
        
        public ManualShould(HttpFixture fixture)
        {
            _fixture = fixture;

        }

        [Fact]
        public async Task ManualRun()
        {
            var actualResult = await _fixture.GetAsync<CupOfCoffeeModel>("/brew-coffee");

            Assert.NotNull(actualResult);
            Assert.Equal("Your piping hot coffee is ready", actualResult.Message);
        }
    }
}
