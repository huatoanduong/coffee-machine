﻿using System;
using System.Runtime.Serialization;

namespace CoffeeMachine.Core.Common.Exceptions
{
    [Serializable]
    public class ForbiddenException : HttpStatusCodeException
    {
        public ForbiddenException() : this("Access was forbidden")
        {
        }

        public ForbiddenException(string message) : base(message)
        {
        }

        public ForbiddenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ForbiddenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override int HttpStatusCode => (int)System.Net.HttpStatusCode.Forbidden;
    }
}
