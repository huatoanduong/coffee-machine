﻿using Newtonsoft.Json;

namespace CoffeeMachine.Core.Common.Exceptions
{
    public class ErrorItem
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
