﻿using System;
using System.Runtime.Serialization;

namespace CoffeeMachine.Core.Common.Exceptions
{
    [Serializable]
    public class MethodNotAllowedException : HttpStatusCodeException
    {
        public MethodNotAllowedException() : this("Method not found")
        {
        }

        public MethodNotAllowedException(string message) : base(message)
        {
        }

        public MethodNotAllowedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MethodNotAllowedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override int HttpStatusCode => (int)System.Net.HttpStatusCode.MethodNotAllowed;
    }
}
