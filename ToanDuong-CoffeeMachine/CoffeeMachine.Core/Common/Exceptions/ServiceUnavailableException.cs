﻿using System;
using System.Runtime.Serialization;

namespace CoffeeMachine.Core.Common.Exceptions
{
    [Serializable]
    public class ServiceUnavailableException : HttpStatusCodeException
    {
        public ServiceUnavailableException() : this("Service Unavailable")
        {
        }

        public ServiceUnavailableException(string message) : base(message)
        {
        }

        public ServiceUnavailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ServiceUnavailableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override int HttpStatusCode => (int)System.Net.HttpStatusCode.ServiceUnavailable;
    }
}
