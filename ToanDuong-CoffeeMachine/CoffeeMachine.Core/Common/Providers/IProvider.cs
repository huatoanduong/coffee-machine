﻿using System.Threading;
using System.Threading.Tasks;

namespace CoffeeMachine.Core.Common.Providers
{
    public interface IProvider<TEntity>
    {
        Task<TEntity> ProvideAsync(CancellationToken cancellationToken = default);
    }
}
