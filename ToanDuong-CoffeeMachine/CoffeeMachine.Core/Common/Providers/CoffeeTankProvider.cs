﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoffeeMachine.Core.Common.Model;
using Microsoft.Extensions.Logging;

namespace CoffeeMachine.Core.Common.Providers
{
    public interface ICoffeeTankProvider : IProvider<int>
    {
    }

    public class CoffeeTankProvider : ICoffeeTankProvider
    {
        private readonly ILogger<CoffeeTankProvider> _logger;
        private readonly CoffeeTankModel _tank;

        public CoffeeTankProvider(
            ILogger<CoffeeTankProvider> logger
        )
        {
            _logger = logger;
            _tank = new CoffeeTankModel
            {
                NumberOfCupsLeft = 4
            };
        }

        public async Task<int> ProvideAsync(CancellationToken cancellationToken = default)
        {
            lock (_tank)
            {
                var numberOfCupsLeft = _tank.NumberOfCupsLeft--;

                if (_tank.NumberOfCupsLeft < 0)
                {
                    _tank.NumberOfCupsLeft = 4;
                }

                return numberOfCupsLeft;
            }
        }
    }
}
