﻿using System.Text;

namespace CoffeeMachine.Core.Common.Configurations
{
    public class TokenSettings
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public long AccessTokenExpirationInMinutes { get; set; }
        public long RefreshTokenExpirationInMinutes { get; set; }
        public string Secret { get; set; }

        public byte[] SecretBytes => Encoding.UTF8.GetBytes(Secret);
    }
}
