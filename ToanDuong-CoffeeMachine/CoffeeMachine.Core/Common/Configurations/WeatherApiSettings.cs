﻿namespace CoffeeMachine.Core.Common.Configurations
{
    public class WeatherApiSettings
    {
        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
        public string CityName { get; set; }
    }
}
