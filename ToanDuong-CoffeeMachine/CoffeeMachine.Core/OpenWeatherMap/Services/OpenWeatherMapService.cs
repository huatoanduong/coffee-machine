﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CoffeeMachine.Core.Common.Configurations;
using CoffeeMachine.Core.Common.Exceptions;
using CoffeeMachine.Core.OpenWeatherMap.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace CoffeeMachine.Core.OpenWeatherMap.Services
{
    public interface IOpenWeatherMapService
    {
        Task<OpenWeatherCityResponseModel> GetCurrentWeatherAsync(CancellationToken cancellationToken);
    }

    public class OpenWeatherMapService : IOpenWeatherMapService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly WeatherApiSettings _settings;

        public OpenWeatherMapService(IHttpClientFactory clientFactory
            , IOptions<WeatherApiSettings> setting
        )
        {
            _settings = setting.Value;
            _clientFactory = clientFactory;
        }


        public async Task<OpenWeatherCityResponseModel> GetCurrentWeatherAsync(CancellationToken cancellationToken)
        {
            var requestUrl = _settings.BaseUrl;
            var cityName = _settings.CityName;
            var apiKey = _settings.ApiKey;

            var queryParam = new Dictionary<string, string>
            {
                {
                    "units", "metric"
                },
                {
                    "appid", apiKey
                },
                {
                    "q", WebUtility.UrlEncode(cityName)
                }
            };

            var queryString = "?" + string.Join("&", queryParam.Select(p => $"{p.Key}={p.Value}").ToList());
            requestUrl += queryString;

            using (var client = _clientFactory.CreateClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUrl))
            {
                var response = await SendRequestAsync<OpenWeatherCityResponseModel>(client, request, cancellationToken);
                return response;
            }
        }

        private async Task<T> SendRequestAsync<T>(HttpClient client, HttpRequestMessage request, CancellationToken cancellationToken)
        {
            using (var response = await client.SendAsync(request, cancellationToken))
            {
                var responseString = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    var result = JsonConvert.DeserializeObject<T>(responseString);
                    return result;
                }

                throw new ServiceUnavailableException();
            }
        }
    }
}
