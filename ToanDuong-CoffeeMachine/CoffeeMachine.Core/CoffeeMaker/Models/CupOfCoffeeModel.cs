﻿using System;

namespace CoffeeMachine.Core.CoffeeMaker.Models
{
    public class CupOfCoffeeModel
    {
        public string Message { get; set; } = "Your piping hot coffee is ready";

        public DateTimeOffset Prepared { get; set; }
    }
}
