﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CoffeeMachine.Core.CoffeeMaker.Models;
using CoffeeMachine.Core.Common.Exceptions;
using CoffeeMachine.Core.Common.Providers;
using CoffeeMachine.Core.OpenWeatherMap.Services;
using MediatR;

namespace CoffeeMachine.Core.CoffeeMaker.Requests
{
    /// <summary>
    /// Separated domains & classes for easier implementation of Micro-Services architecture
    /// </summary>
    public class MakeCoffee
    {
        public class Request : IRequest<CupOfCoffeeModel>
        {
        }

        public class Handler : IRequestHandler<Request, CupOfCoffeeModel>
        {
            private readonly ICoffeeTankProvider _coffeeTankProvider;
            private readonly IOpenWeatherMapService _openWeatherMapService;

            public Handler(
                ICoffeeTankProvider coffeeTankProvider
                , IOpenWeatherMapService openWeatherMapService
            )
            {
                _coffeeTankProvider = coffeeTankProvider;
                _openWeatherMapService = openWeatherMapService;
            }

            public async Task<CupOfCoffeeModel> Handle(Request request, CancellationToken cancellationToken)
            {
                // We can maintain the logic of making coffee here
                // It helps code centralized and easier to maintain

                var numberOfCupsLeft = await _coffeeTankProvider.ProvideAsync(cancellationToken);

                if (numberOfCupsLeft <= 0)
                {
                    throw new ServiceUnavailableException();
                }

                var message = "Your piping hot coffee is ready";

                var currenCityWeather = await _openWeatherMapService.GetCurrentWeatherAsync(cancellationToken);

                if ((currenCityWeather?.Main?.Temp).HasValue && (currenCityWeather.Main.Temp > 30))
                {
                    message = "Your refreshing iced coffee is ready";
                }

                return new CupOfCoffeeModel
                {
                    Message = message,
                    Prepared = DateTimeOffset.Now
                };
            }
        }
    }
}
